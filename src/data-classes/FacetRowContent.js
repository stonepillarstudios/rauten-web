export default class FacetRowContent {
    title;
    facets;
    bgImg;
    constructor(title, facets, bgImg=null) {
        this.title = title;
        this.facets = facets;
        this.bgImg = bgImg;
    }
}
