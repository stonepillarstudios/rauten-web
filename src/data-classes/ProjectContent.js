export default class ProjectContent {
    id;
    image;
    name;
    status;
    lastUpdated;
    desc;
    techs;
    website;
    playstore;
    vcsID;
    loc;

    // calculated
    links;

    constructor(data) {
        this.id = data.id;
        this.image =  data.image;
        this.name = data.name;
        this.status = data.status;
        this.lastUpdated = new Date(data.lastUpdated);
        this.desc = data.description;
        this.techs = data.techs;
        this.website = data.website;
        this.playstore = data.playstore;

        this.loc = data.loc; // nullable
        if (this.loc){
            const num = this.loc.valueOf();
            if (num >= 1000){
                const k = Math.round(num / 1000);
                this.loc = '~' + k + '&nbsp;000'
            }else{
                this.loc = '<1&nbsp;000'
            }
        }else this.loc = '';

        this.links = [];
        if (data.website)
            this.links.push({link: data.website, icon: 'website.png'});
        if (data.playstore)
            this.links.push({link: data.playstore, icon: 'play.png'});
        if (data.vcs){
            this.vcsID = data.vcs.id;
            if (data.vcs.weblink.startsWith('https://bitbucket.org'))
                this.links.push({link: data.vcs.weblink,
                    icon: data.vcs.public ? 'bitbucket.svg' : 'bitbucket_private.png'});

            else if (data.vcs.weblink.startsWith('https://github.com'))
                this.links.push({link: data.vcs.weblink,
                    icon: data.vcs.public ? 'github.svg' : 'github_private.png'});

            else if (data.vcs.weblink.startsWith('https://gitlab.com'))
                this.links.push({link: data.vcs.weblink,
                    icon: data.vcs.public ? 'gitlab.png' : 'gitlab_private.png'});
        }
    }
}
