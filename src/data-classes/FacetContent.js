export default class FacetContent {
    name;
    image;
    link;
    desc;
    isCharity;

    constructor(name, desc, imageUrl, link, isCharity=false) {
        this.name = name;
        this.image = imageUrl;
        this.link = link;
        this.desc = desc;
        this.isCharity = isCharity;
    }
}
