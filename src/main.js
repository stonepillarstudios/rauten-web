import Vue from 'vue'
import VueRouter from "vue-router";
import {BootstrapVue} from 'bootstrap-vue';
import VueScrollTo from 'vue-scrollto';
import {
  MdTable,
  MdContent,
  MdProgress,
  MdField,
  MdCheckbox,
  MdButton,
  MdSnackbar,
  MdRipple, MdSwitch, MdTooltip, MdDialog, MdAvatar,
  MdTabs
} from 'vue-material/dist/components';
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import Home from "@/components/home/Home";
import Unabridged from "@/components/Unabridged";
import NotFound from "@/components/NotFound";
import Projects from "@/components/Projects";
import Charity from "./components/Charity";
import Dashboard from "./components/Dashboard";

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(VueScrollTo);

// specific material components
Vue.use(MdTable);
Vue.use(MdContent);
Vue.use(MdProgress);
Vue.use(MdField);
Vue.use(MdCheckbox);
Vue.use(MdButton);
Vue.use(MdSnackbar);
Vue.use(MdRipple);
Vue.use(MdSwitch);
Vue.use(MdTooltip);
Vue.use(MdAvatar);
Vue.use(MdDialog);
Vue.use(MdTabs);

// Changes more than you'd think
const myName = 'Jans Rautenbach';

const routes = [
    ['/', Home, myName, 'Website of '+myName],
    ['/unabridged', Unabridged, myName+' - Unabridged CV', "Unabridged CV of "+myName],
    ['/projects', Projects, myName+' - Personal Projects', "Personal Projects of "+myName],
    ['/social-responsibility', Charity, myName+' - Social Responsibility', 'NGO\'s and charities supported by '+myName],
    ['/dashboard', Dashboard, 'Rauten Dashboard', 'Public rauten.co.za content management dashboard'],
    ['*', NotFound, 'Not Found', '404'],
].map(tuple => ({
  path: tuple[0],
  component: tuple[1],
  meta: {
    title: tuple[2],
    metaTags: [{
      name: 'description',
      content: tuple[3]
    },{
      property: 'og:description',
      content: tuple[3]
    }]
  },
}));

const router = new VueRouter({
  routes,
  mode: 'history',
});

// This callback runs before every route change, including on page load.
router.beforeEach((to, from, next) => {
  // This goes through the matched routes from last to first, finding the closest route with a title.
  // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

  // Skip rendering meta tags if there are none.
  if(!nearestWithMeta) return next();

  // Turn the meta tag definitions into actual elements in the head.
  nearestWithMeta.meta.metaTags.map(tagDef => {
    const tag = document.createElement('meta');

    Object.keys(tagDef).forEach(key => {
      tag.setAttribute(key, tagDef[key]);
    });

    // We use this to track which meta tags we create, so we don't interfere with other ones.
    tag.setAttribute('data-vue-router-controlled', '');

    return tag;
  })
  // Add the meta tags to the document head.
      .forEach(tag => document.head.appendChild(tag));

  next();
});

new Vue({
  router,
}).$mount('#app');


