# Rauten website

Source code for [rauten.co.za](https://rauten.co.za).

## Config

In `.env.[mode]`. Mode is `dev`, `staging`, or `production`.

App should be built by specifing the correct `--mode`.
